package com.connection.pool.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class City {

    @Id
    private Long ID;

    private String name;

    private String CountryCode;

}
