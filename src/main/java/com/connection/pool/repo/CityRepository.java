package com.connection.pool.repo;

import com.connection.pool.entities.City;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "cities")
public interface CityRepository extends CrudRepository<City,Long> {
}
