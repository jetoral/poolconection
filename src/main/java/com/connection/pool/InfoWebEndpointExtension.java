package com.connection.pool;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.web.annotation.EndpointWebExtension;
import org.springframework.boot.actuate.health.*;
import org.springframework.boot.actuate.info.InfoEndpoint;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@EndpointWebExtension(endpoint = InfoEndpoint.class)
public class InfoWebEndpointExtension {

    @Autowired
    private HealthEndpoint healthEndpoint;

    static final Health DEFAULT_HEALTH = Health.up().build();

    @SneakyThrows
    @ReadOperation
    public HealthComponent health() {
        HealthComponent health = this.healthEndpoint.health();
        if (health!=null) {
            if (health.getStatus().equals(Status.DOWN)){
                throw new MysqlExceptionPool();
            }
        }
        return (HealthComponent)(health != null ? health : DEFAULT_HEALTH);
    }

}
