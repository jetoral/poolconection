package com.connection.pool;

import javax.sql.DataSource;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.util.DriverDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Configuration
@Component
public class DataSourceConfig {

    @Value("${spring.datasource.driver-class-name:empty}")
    String driverClassName;
    @Value("${spring.datasource.url:empty}")
    String dataSourceUrl;

    @Value("${spring.datasource.username:empty}")
    String userName;
    @Value("${spring.datasource.password:empty}")
    String password;

    @Bean
    public DataSource getDataSource() {
        DataSourceBuilder<?> dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(this.driverClassName);
        dataSourceBuilder.url(this.dataSourceUrl);
        dataSourceBuilder.username(this.userName);
        dataSourceBuilder.password(this.password);

        return dataSourceBuilder.build();
    }

}
