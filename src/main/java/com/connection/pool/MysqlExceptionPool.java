package com.connection.pool;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class MysqlExceptionPool extends Exception {

    public MysqlExceptionPool() {
        this("Error al conectar con la base de datos!");
    }

    public MysqlExceptionPool(String message) {
        this(message, null);
    }

    private MysqlExceptionPool(String message, Throwable cause) {
        super(message, cause);
    }

}